import React from 'react'
// import route Components here
import {
  Route,
  Redirect
} from 'react-router-dom'
import {fakeAuth} from './LoginComponent'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    fakeAuth.isAuthenticated ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

export default PrivateRoute
