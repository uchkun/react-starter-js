import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import './App.css'
import LoginComponent from './LoginComponent'
import PrivateRoute from './PrivateRoute'
import AdminComponent from './AdminComponent'

export const App = () => {
  return (
    <div className="App">
      <Router>
        <div>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a href="#" className="navbar-brand">Brand</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" area-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active"><Link to="/" className="nav-link">Home <span className="sr-only">(current)</span></Link></li>
                <li className="nav-item"><Link to="/books" className="nav-link">Books</Link></li>
                <li className="nav-item"><Link to="/about" className="nav-link">About</Link></li>
                <li className="nav-item"><Link to="/admin" className="nav-link">Admin Page</Link></li>
              </ul>
            </div>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item"><Link to="/login" className="nav-link">Login</Link></li>
              </ul>
            </div>
          </nav>

          <div className="container">
            <Route path="/" exact={true} render={() => { return <div className="jumbotron"><h2>Welcome page</h2></div> }}/>
            <Route path="/books" render={() => { return <div className="jumbotron"><h2>Books page</h2></div> }}/>
            <Route path="/about" render={() => { return <div className="jumbotron"><h2>About page</h2></div> }}/>
            <Route path="/login" component={LoginComponent}/>
            <PrivateRoute path="/admin" component={AdminComponent}/>
          </div>
        </div>
      </Router>
    </div>
  )
}

export default App
